# API que expõe os dados presentes no log do quake arena

# Tecnologias 
* Java 8
* Spring Boot
* Redus
* Docker

# Pré-Requisitos
* Linux ou Mac
* JDK 1.8
* Maven 3.0+
* Docker

# Endpoints
Retorna a lista de games e seus respectivos detalhes
```
GET /games/details

[
    {
        "game_1": {
            "totalKills": 67,
            "players": [
                "Dono da Bola",
                "Chessus",
                "Assasinu Credi",
                "Mal",
                "Chessus!",
                "Isgalamido",
                "Zeh",
                "Oootsimo"
            ],
            "kills": {
                "Dono da Bola": 2,
                "Chessus": 8,
                "Assasinu Credi": 10,
                "Mal": 3,
                "Chessus!": 0,
                "Isgalamido": 1,
                "Zeh": 12,
                "Oootsimo": 9
            }
        }
    },
    {
        "game_2": {
            "totalKills": 95,
            "players": [
                "Dono da Bola",
                "Assasinu Credi",
                "Mal",
                "Isgalamido",
                "Zeh",
                "Oootsimo"
            ],
            "kills": {
                "Dono da Bola": 14,
                "Assasinu Credi": 9,
                "Mal": 2,
                "Isgalamido": 14,
                "Zeh": 20,
                "Oootsimo": 10
            }
        }
    }
 ]

```

Retorna os detalhes de um game específico
```
GET /games/details/<identificador do game>

{
    "game_1": {
        "totalKills": 67,
        "players": [
            "Dono da Bola",
            "Chessus",
            "Assasinu Credi",
            "Mal",
            "Chessus!",
            "Isgalamido",
            "Zeh",
            "Oootsimo"
        ],
        "kills": {
            "Dono da Bola": 2,
            "Chessus": 8,
            "Assasinu Credi": 10,
            "Mal": 3,
            "Chessus!": 0,
            "Isgalamido": 1,
            "Zeh": 12,
            "Oootsimo": 9
        }
    }
}

```

Retorna todos os identificadores dos games
```
GET /games/names

[
    "game_1",
    "game_2"
]
```

# Execução
* Executar o script run.sh presente na raiz do projeto
* Este script criará a imagen docker ruhan/quake-log-api e subirá o container correspondente, além de subir o redis
* A api estará disponível na porta 8080

# Testes
* Foram desenvolvidos com o framework do Spring Boot
* São executados na fase de criação da imagem docker