#!/bin/sh
set -e -x

sudo true

mvn clean install dockerfile:build

docker stop redis-server-quake && docker rm redis-server-quake
docker stop quake-log-api && docker rm quake-log-api

docker run -d -p 6379:6379 --name redis-server-quake redis
docker run -d --name quake-log-api -p 8080:8080 --link redis-server-quake:redis-server-quake ruhan/quake-log-api