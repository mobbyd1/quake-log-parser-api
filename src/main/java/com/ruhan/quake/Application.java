package com.ruhan.quake;

import com.ruhan.quake.dlo.GameDLO;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * Created by ruhandosreis on 21/11/17.
 */

@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        final ConfigurableApplicationContext applicationContext = SpringApplication.run(Application.class, args);

        final GameDLO gameDLO = applicationContext.getBean(GameDLO.class);
        gameDLO.init("games.log");
    }
}
