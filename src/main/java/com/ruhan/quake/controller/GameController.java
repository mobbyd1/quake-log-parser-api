package com.ruhan.quake.controller;

import com.ruhan.quake.dlo.GameDLO;
import com.ruhan.quake.model.Game;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by ruhan on 21/11/17.
 */
@Controller
public class GameController extends AbstractControllerWithErrorHandling {

    @Autowired
    GameDLO gameDLO;

    @RequestMapping(value = "/games/details", method = RequestMethod.GET)
    public ResponseEntity getAll() {
        final List<Map<String, Game>> all = gameDLO.getAll();
        return new ResponseEntity( all, HttpStatus.OK );
    }

    @RequestMapping(value = "/games/details/{id}", method = RequestMethod.GET)
    public ResponseEntity getGameDetail(@PathVariable String id) {
        final Map<String, Game> game = gameDLO.get(id);
        return new ResponseEntity( game, HttpStatus.OK );
    }

    @RequestMapping(value = "/games/names", method = RequestMethod.GET)
    public ResponseEntity getGamesNames() {
        final Set<String> gamesNames = gameDLO.getGamesNames();
        return new ResponseEntity( gamesNames, HttpStatus.OK );
    }
}
