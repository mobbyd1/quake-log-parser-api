package com.ruhan.quake.dao;

import com.ruhan.quake.model.Game;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by ruhan on 21/11/17.
 */
public interface GameDAO {

    /**
     * Init the persistence service
     */
    void init();

    /**
     *
     * @param key
     * @param game
     */
    void save( String key, Game game );

    /**
     * Get all saved objects
     * @return list key-value
     */
    List<Map<String, Game>> getAll();

    /**
     * Get value based on its key
     * @param key
     * @return
     */
    Map<String, Game> get( String key );

    /**
     * Get all keys
     * @return
     */
    Set<String> getKeys();
}
