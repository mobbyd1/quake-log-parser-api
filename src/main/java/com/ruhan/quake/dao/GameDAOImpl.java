package com.ruhan.quake.dao;

import com.google.gson.Gson;
import com.ruhan.quake.model.Game;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;

import java.util.*;

/**
 * Created by ruhan on 21/11/17.
 */
@Component
public class GameDAOImpl implements GameDAO {

    @Value("${spring.redis.host}")
    private String host;

    @Value("${spring.redis.port}")
    private int port;

    private Jedis jedis;

    public void init() {
        jedis = new Jedis(host, port);
    }

    public void save(final String key, final Game game) {
        final Gson gson = new Gson();
        final String json = gson.toJson(game);

        jedis.set( key, json );
    }

    @Override
    public List<Map<String, Game>> getAll() {
        final Gson gson = new Gson();
        final Set<String> keys = getKeys();

        final List<Map<String, Game>> values = new ArrayList<>();

        for( final String key : keys ) {
            final String value = jedis.get( key );

            final Game game = gson.fromJson(value, Game.class);

            final Map<String, Game> map = new HashMap<>();
            map.put( key, game );

            values.add( map );
        }

        return values;
    }

    public Map<String, Game> get(String key) {
        final String value = jedis.get( key );

        final Gson gson = new Gson();
        final Game game = gson.fromJson(value, Game.class);

        final Map<String, Game> map = new HashMap<>();
        map.put( key, game );

        return map;
    }

    public Set<String> getKeys() {
        return jedis.keys("*");
    }
}
