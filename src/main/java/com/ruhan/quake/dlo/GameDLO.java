package com.ruhan.quake.dlo;

import com.ruhan.quake.model.Game;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by ruhan on 21/11/17.
 */
public interface GameDLO {

    /**
     * Load the log file and save the contents
     */
    void init(String logFile);

    /**
     * Get all saved objects
     * @return list of key-value
     */
    List<Map<String, Game>> getAll();

    /**
     * Get value based on its key
     * @param key
     * @return
     */
    Map<String, Game> get(String key);

    /**
     * Get all games names
     * @return
     */
    Set<String> getGamesNames();
}
