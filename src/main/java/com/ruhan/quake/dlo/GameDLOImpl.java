package com.ruhan.quake.dlo;

import com.google.gson.Gson;
import com.ruhan.quake.dao.GameDAO;
import com.ruhan.quake.model.Game;
import com.ruhan.quake.parser.ParserDLO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by ruhan on 21/11/17.
 */
@Component
public class GameDLOImpl implements GameDLO {

    @Autowired
    ParserDLO parserDLO;

    @Autowired
    GameDAO gameDAO;

    public void init( final String logFile ) {
        gameDAO.init();

        final List<Game> games = parserDLO.parse(logFile);

        for( final Game game : games )  {
            final String name = game.getName();
            gameDAO.save( name, game );
        }
    }

    public List<Map<String, Game>> getAll() {
        return gameDAO.getAll();
    }

    public Map<String, Game> get(final String key) {
        return gameDAO.get( key );
    }

    public Set<String> getGamesNames() {
        return gameDAO.getKeys();
    }

}
