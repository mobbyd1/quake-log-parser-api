package com.ruhan.quake.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by ruhandosreis on 21/11/17.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Game {

    private transient String name;
    private int totalKills;
    private Set<String> players;
    private Map<String, Integer> kills;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTotalKills() {
        return totalKills;
    }

    public void setTotalKills(int totalKills) {
        this.totalKills = totalKills;
    }

    public Map<String, Integer> getKills() {
        return kills;
    }

    public void setKills(Map<String, Integer> kills) {
        this.kills = kills;

        final Set<String> players = kills.keySet();
        setPlayers( players );
    }

    public Set<String> getPlayers() {
        return players;
    }

    public void setPlayers(Set<String> players) {
        this.players = players;
    }
}
