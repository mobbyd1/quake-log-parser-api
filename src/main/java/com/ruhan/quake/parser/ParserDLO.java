package com.ruhan.quake.parser;

import com.ruhan.quake.model.Game;

import java.util.List;

/**
 * Created by ruhandosreis on 21/11/17.
 */
public interface ParserDLO {

    /**
     * Parse the specified log file
     * @param logFileName
     * @return a list of games
     */
    List<Game> parse( String logFileName );
}
