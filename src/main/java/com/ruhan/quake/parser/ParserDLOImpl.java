package com.ruhan.quake.parser;

import com.ruhan.quake.model.Game;
import com.ruhan.quake.util.LoadFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ruhandosreis on 21/11/17.
 */
@Component
public class ParserDLOImpl implements ParserDLO {

    private final Logger LOG = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private LoadFile loadFile;

    private static final String WORLD = "<world>";
    private static final String PATTERN_CLIENT = "\\s*\\d{1,2}:\\d{2}\\s*(ClientUserinfoChanged)\\w*";
    private static final String PATTERN_INIT_GAME = "\\s*\\d{1,2}:\\d{2}\\s*(InitGame)\\w*";
    private static final String PATTERN_END_GAME = "\\s*\\d{1,2}:\\d{2}\\s*(ShutdownGame)\\w*";
    private static final String PATTERN_KILL = "\\s*\\d{1,2}:\\d{2}\\s*(Kill)\\w*";

    @Override
    public List<Game> parse(String logFileName) {

        final List<Game> games = new ArrayList<>();

        /* Regex para verificar o conteúdo inicial de cada linha */
        final Pattern patternClient = Pattern.compile( PATTERN_CLIENT );
        final Matcher matcherClient = patternClient.matcher( "" );

        final Pattern patternInitGame = Pattern.compile( PATTERN_INIT_GAME );
        final Matcher matcherInitGame = patternInitGame.matcher( "" );

        final Pattern patternEndGame = Pattern.compile( PATTERN_END_GAME );
        final Matcher matcherEndGame = patternEndGame.matcher( "" );

        final Pattern patternKill = Pattern.compile( PATTERN_KILL );
        final Matcher matcherKill = patternKill.matcher( "" );

        try {

            final InputStream inputStream = loadFile.getInputStream(logFileName);
            final BufferedReader bufferedReader
                    = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));

            int gamesCounter = 0;
            String currentLine;

            Game game = null;
            Map<String, Integer> killsForPlayers = null;
            Integer totalKills = null;

            while( ( currentLine = bufferedReader.readLine() ) != null ) {

                currentLine = currentLine.trim();

                matcherKill.reset( currentLine );// Passa a linha para o regex verificar
                matcherInitGame.reset( currentLine );
                matcherEndGame.reset( currentLine );
                matcherClient.reset( currentLine );

                if( matcherInitGame.find() ) {

                    gamesCounter++;

                    // finaliza o jogo anterior, se não terminou, e inicializa um novo

                    if ( game != null && !games.contains( game ) ) {
                        game.setTotalKills( totalKills );
                        game.setKills( killsForPlayers );
                        games.add( game );
                    }

                    game = new Game();

                    totalKills = 0;
                    killsForPlayers = new HashMap<String, Integer>();

                    game.setName( "game_" + gamesCounter );

                } else if( matcherClient.find() ) {

                    // Quebra a linha em partes
                    final String[] split = currentLine.split( "\\\\" );

                    // Adiciona o novo player com kills = 0
                    if ( killsForPlayers.get( split[1] ) == null ) {
                        killsForPlayers.put(split[1], 0);
                    }

                } else if( matcherKill.find() ) {

                    totalKills++;

                    final String[] split = currentLine.split( "killed" );
                    final String[] splitFirstPlayer = split[0].split( ":" );
                    final String[] splitSecondPlayer = split[1].split( "by" );

                    final String firstPlayer = splitFirstPlayer[splitFirstPlayer.length - 1].trim();
                    final String secondPlayer = splitSecondPlayer[0].trim();

                    if ( firstPlayer.equalsIgnoreCase( WORLD ) ) {
                        final int kills = killsForPlayers.get(secondPlayer);
                        killsForPlayers.put( secondPlayer, kills - 1 );
                    }

                    else {
                        final Integer kills = killsForPlayers.get(firstPlayer);
                        killsForPlayers.put( firstPlayer, kills.intValue() + 1 );
                    }

                } else if( matcherEndGame.find() ) {
                    if( game != null ) {
                        game.setTotalKills(totalKills);
                        game.setKills(killsForPlayers);
                        games.add(game);
                    }
                }
            }

            return games;

        } catch ( IOException e ) {
            final String msg = String.format("Ocorreu um erro ao parsear o arquivo %s", logFileName);
            LOG.error( msg, e );

            return Collections.EMPTY_LIST;
        }
    }
}
