package com.ruhan.quake.util;

import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by ruhandosreis on 21/11/17.
 */
@Component
public class LoadFile {

    public InputStream getInputStream( final String fileName ) throws IOException {
        return new ClassPathResource(fileName).getInputStream();
    }
}
