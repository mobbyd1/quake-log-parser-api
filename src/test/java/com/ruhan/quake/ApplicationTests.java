package com.ruhan.quake;

import com.ruhan.quake.dao.GameDAO;
import com.ruhan.quake.dlo.GameDLO;
import com.ruhan.quake.model.Game;
import com.ruhan.quake.parser.ParserDLO;
import com.ruhan.quake.util.LoadFile;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import javax.annotation.PreDestroy;
import java.io.IOException;
import java.io.InputStream;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by ruhan on 21/11/17.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ApplicationTests {

    @Autowired
    GameDLO gameDLO;

    @Autowired
    MockMvc mockMvc;

    @Value("${spring.redis.port}")
    private int redisPort;

    private redis.embedded.RedisServer redisServer;

    @Before
    public void init() throws Exception {
        redisServer = new redis.embedded.RedisServer(redisPort);
        redisServer.stop();
        redisServer.start();
    }

    @After
    public void stopRedis() {
        this.redisServer.stop();
    }


    @Test
    public void test1() throws Exception {
        gameDLO.init( "logs/games-1.log" );

        mockMvc.perform(MockMvcRequestBuilders.get("/games/details"))
                .andExpect( status().isOk() )
                .andExpect( jsonPath( "$", hasSize(1) ) )
                .andExpect( jsonPath( "$[0].game_1.totalKills", is(0) ) )
                .andExpect( jsonPath( "$[0].game_1.players.length()", is(1) ) )
                .andExpect( jsonPath( "$[0].game_1.players[0]", is("Isgalamido") ) )
                .andExpect( jsonPath( "$[0].game_1.kills.Isgalamido", is(0) ) );
    }

    @Test
    public void test2() throws Exception {
        gameDLO.init( "logs/games-2.log" );

        mockMvc.perform(MockMvcRequestBuilders.get("/games/details"))
                .andExpect( status().isOk() )
                .andExpect( jsonPath( "$", hasSize(1) ) )
                .andExpect( jsonPath( "$[0].game_1.totalKills", is(1) ) )
                .andExpect( jsonPath( "$[0].game_1.kills.Isgalamido", is(-1) ) );
    }

    @Test
    public void test3() throws Exception {
        gameDLO.init( "logs/games-3.log" );

        mockMvc.perform(MockMvcRequestBuilders.get("/games/details"))
                .andExpect( status().isOk() )
                .andExpect( jsonPath( "$", hasSize(1) ) )
                .andExpect( jsonPath( "$[0].game_1.players.length()", is(2) ) )
                .andExpect( jsonPath( "$[0].game_1.totalKills", is(3) ) )
                .andExpect( jsonPath( "$[0].game_1.kills.Isgalamido", is(2) ) )
                .andExpect( jsonPath( "$[0].game_1.kills.['Dono da Bola']", is(1) ) );
    }

    @Test
    public void test4() throws Exception {
        gameDLO.init( "logs/games-4.log" );

        mockMvc.perform(MockMvcRequestBuilders.get("/games/details/game_1"))
                .andExpect( status().isOk() )
                .andExpect( jsonPath( "$.game_1.totalKills", is(5) ) )
                .andExpect( jsonPath( "$.game_1.players.length()", is(2) ) )
                .andExpect( jsonPath( "$.game_1.kills.Isgalamido", is(0) ) )
                .andExpect( jsonPath( "$.game_1.kills.['Dono da Bola']", is(1) ) );
    }

    @Test
    public void test5() throws Exception {
        gameDLO.init( "logs/games-5.log" );

        mockMvc.perform(MockMvcRequestBuilders.get("/games/details/game_2"))
                .andExpect( status().isOk() )
                .andExpect( jsonPath( "$.game_2.totalKills", is(3) ) )
                .andExpect( jsonPath( "$.game_2.players.length()", is(2) ) )
                .andExpect( jsonPath( "$.game_2.kills.Isgalamido", is(-2) ) )
                .andExpect( jsonPath( "$.game_2.kills.['Dono da Bola']", is(1) ) );

    }

    @Test
    public void test6() throws Exception {
        gameDLO.init( "logs/games-5.log" );

        mockMvc.perform(MockMvcRequestBuilders.get("/games/names"))
                .andExpect( status().isOk() )
                .andExpect( jsonPath( "$", hasSize(2) ) );
    }

    @Test
    public void test7() throws Exception {
        gameDLO.init( "123" );

        mockMvc.perform(MockMvcRequestBuilders.get("/games/names"))
                .andExpect( status().isOk() )
                .andExpect( jsonPath( "$", hasSize(0) ) );
    }
}
